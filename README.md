# wp-target-blank
A lightweight WordPress plugin that uses JQuery to add `target="_blank"` and `rel="noopener"` to external URLs. Certain file formats that commonly opened within the browser are also included (currently only `PDF` and `TXT`).

## Why is `rel="noopener"` being used?
Basically for two reasons: performance and security. With out using `noopener` the new page runs on the same process as your page. This also means the new page has access to your window object and can then use JS to redirect your page to another URL... so basically a new page/window opens and while the user isn't looking the new site they just opened could load a malicious website on the tab/window your site *was* on. For more details see the explenation on [developers.google.com](https://developers.google.com/web/tools/lighthouse/audits/noopener).

## NOTE
To avoid conflicts with themes that are using jQuery, this plugin does not enqueue jQuery. If your site is not using jQuery, version 2.2.2 is included with this plugin. To use the version of jQuery included with this plugin, uncomment the linein `wp-target-blank.php` shown below.

``` // wp_enqueue_script( 'wp-target-blank-jquery', plugins_url('/wp-target-blank/js/jquery.min.js') ); ```
