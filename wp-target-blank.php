<?php
/*
Plugin Name:  WP Target Blank
Description:  Opens links in a new tab/window if the URL is outside of the website's domain as well as local documents (e.g. PDF, TXT)
Plugin URI:   https://github.com/webunraveling/wp-target-blank
Version:      1.1
Author:       Jason Raveling
Author URI:   http://webunraveling.com
License:      GPL2
License URI:  https://www.gnu.org/licenses/gpl-2.0.html

/*
WP Target Blank
Copyright (C) 2016 Jason Raveling

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
*/

add_action( 'wp_enqueue_scripts', 'wp_target_blank_init' );

function wp_target_blank_init() {
	// If you aren't already using jQuery on your site, uncomment the line below.
	// wp_enqueue_script( 'wp-target-blank-jquery', plugins_url('/wp-target-blank/js/jquery.min.js') );
	wp_enqueue_script( 'wp-target-blank', plugins_url('/wp-target-blank/js/wp-target-blank.js'), array( 'jquery' ), 1.0, true );
	wp_enqueue_script( 'wp-target-blank-psl', plugins_url('/wp-target-blank/js/psl.min.js'), array( 'jquery' ), 1.0, true );
}
