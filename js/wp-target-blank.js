(function($) { $(document).ready(function() {

  jQuery("a").each(function(){

    var host = psl.get(location.host);
    var link = $(this).attr('href');

    // regex to be tested
    var isHost = new RegExp( host, 'gi');
    var isDoc = new RegExp('\.(pdf|txt)', 'gi');
    var isAnchor = new RegExp('(#){1}[a-z0-9-_]*', 'gi');
    var isProtocol = new RegExp('(:\/\/)', 'gi');

    if ( isDoc.test(link) ) {
      var external = true;
    } else if ( isHost.test(link) ) {
      var external = false;
    } else if ( isProtocol.test(link) && !isHost.test(link) ) {
      var external = true;
    } else {
      var external = false;
    }

    if ( external ) {
        $(this).attr( "target", "_blank" );
        $(this).attr( "rel", "noopener" );
    }

  });

}); })(jQuery);
